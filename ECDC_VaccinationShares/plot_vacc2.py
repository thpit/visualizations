# Written by Th. Pitschel, 2021-08-07
# Licence: CC-BY-NC-SA  (see https://creativecommons.org/about/cclicenses/) 

# python3 code

# Changelog:
# 

import sys

from datetime import datetime
from datetime import timedelta

import csv
import numpy as np

import matplotlib.pyplot as plt
from matplotlib import ticker




#filter_region = 'DE'
#filter_tgtgroup = 'ALL'
#pop_size = 83000000                 # take the total population count, not only all potential vacc. receivers

filter_region = sys.argv[1]
filter_tgtgroup = 'ALL'
pop_size = int(sys.argv[2])
start_dt = datetime(2020, 1, 1)

infile_path = sys.argv[3]

#-   parser = optparse.OptionParser('%prog [options]')
#-   parser.add_option('-c', '--config', metavar='fn', 
#-      help='use this configuration file or directory')
#:   opts, args = parser.parse_args(argv)


VaccTypeKey = dict()
VaccTypeKey['COM'] = 0
VaccTypeKey['JANSS'] = 1
VaccTypeKey['AZ'] = 2
VaccTypeKey['MOD'] = 3
VaccTypeKeyR = list(VaccTypeKey.keys())
print(VaccTypeKeyR)



# 1. Read data into numpy array
#    store time info as days since base data
# 1b. Compute accumulated counts.
# 2. Transfer data into key-ed format using make_data_dict()
# 3. Apply stackplot


class ECDC_VaccinationDataReader:

    def read_data(self, max_nmb_rows=1000):
        res = np.zeros((max_nmb_rows, 20))

        #infile = "c:/Users/2/Downloads/EU_vaccinations_data_20210807.csv"
        #infile_path = "EU_vaccinations_data_20210807.csv"
        fields_dict = None
        with open(infile_path, 'r', newline='', encoding='utf-8') as f:   
            record_reader = csv.reader(f, delimiter=',', quotechar='"')
            i=0
            ii_max = 0
            for row in record_reader:
                #print(row)
                #print(row)
                #print(len(row))
                if fields_dict is None:
                    fields_dict = dict()
                    for k in range(len(row)):
                        fields_dict[row[k]] = k
                    print(fields_dict)
                if row[fields_dict['Region']] != filter_region: continue
                if row[fields_dict['TargetGroup']] != filter_tgtgroup: continue
                #print(row)
                vacctype = row[fields_dict['Vaccine']]
                yearweekiso = row[0]
                # Week index since 2020-01-01:
                ii = 53*(-2020 + int(yearweekiso[0:4])) + (-1 + int(yearweekiso[6:])) 
                    ##t = dt.datetime(int(yearweekiso[0:4]), 1, 1) + 7*dt.timedelta(days=ii)
                    ##print(t)
                # Day count:
                res[ii, 0] = ii * 7
                ##dates[ii] = dt.datetime.utcfromtimestamp(dt.datetime(2020, 1, 1).timestamp() + ii*86400)
                res[ii,  1 + VaccTypeKey[vacctype]] = row[fields_dict['FirstDose']]
                res[ii, 11 + VaccTypeKey[vacctype]] = row[fields_dict['SecondDose']]
                ii_max = max(ii, ii_max)
            print("I: Filtered rows read: ", ii_max)
        return res, fields_dict, ii_max

    def make_data_dict(self, data, base_index, factor):
        res = dict()
        m,d = data.shape
        keys_ls = VaccTypeKeyR
        for j in range(len(keys_ls)):
            ls1 = []
            for i in range(m):
                ls1.append(data[i, base_index+j] * factor)
            res[keys_ls[j]] = ls1
        return res





x_left = 54
x_right = 83  # to be adjusted to account for expanding data set

dr1 = ECDC_VaccinationDataReader()

data, fields_dict, ii_max = dr1.read_data()
print("ii_max: ", ii_max)
data_acc = data.cumsum(axis=0)
data_acc[:,0] = data[:,0]  # restore time values

d1 = dr1.make_data_dict(data_acc[x_left:x_right, :], 1, 1.0/pop_size)  # first dose counts
d2 = dr1.make_data_dict(data_acc[x_left:x_right, :], 11, 1.0/pop_size)  


fig1 = plt.figure(figsize=(8,5))
ax1 = fig1.add_axes([0.1, 0.08, 0.85, 0.40])
ax2 = fig1.add_axes([0.1, 0.52, 0.85, 0.40])
#ax1 = fig1.subplots()

@ticker.FuncFormatter
def major_formatter(x, pos):
    #print(x)
    return (start_dt + timedelta(days=x)).strftime('%y-%m-%d') #'%Y-%m') 

ax1.xaxis.set_major_formatter(major_formatter)
plt.setp(ax1.get_xticklabels(), rotation=14, ha="center")
ax2.set_xticklabels([])
#ax2.xaxis.set_major_formatter(empty_formatter)
#plt.setp(ax2.get_xticklabels(), rotation=14, ha="center")


ax1.set_ylabel('% of population, 1st dose')
ax2.set_ylabel('% of population, 2nd dose')
#ax1.plot(data[x_left:x_right,0], (data_acc['COM'][x_left:x_right,11] + 0.0)/pop_size, label='Accum. vaccinations, COM')

colors = ['#1f77b490', '#ff7f0e90', '#2ca02c90', '#d6272890']
ax1.stackplot(data[x_left:x_right,0], d1.values(), labels=d1.keys(), colors=colors)
ax2.stackplot(data[x_left:x_right,0], d2.values(), labels=d2.keys(), colors=colors)
ax1.legend(loc='upper left')
ax2.legend(loc='upper left')

plt.title("Population share with vaccination received, {0} (src: ECDC, 2021-08-07)".format(filter_region))

plt.savefig("DE_vaccination_shares_20210808.png")

plt.show()







